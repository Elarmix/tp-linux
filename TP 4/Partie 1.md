# TP4: Automatisation

## Poste de déploiement
###
### Vagrant
```powershell
PS D:\Vagrant\CentOS7> vagrant init centos/7
A `Vagrantfile` has been placed in this directory. You are now
ready to ``vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
``vagrantup.com` for more information on using Vagrant.

PS D:\Vagrant\CentOS7> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'centos/7' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'centos/7'
    default: URL: https://vagrantcloud.com/centos/7
==> default: Adding box 'centos/7' (v2004.01) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/centos/boxes/7/versions/2004.01/providers/virtualbox.box
Download redirected to host: cloud.centos.org
    default:
    default: Calculating and comparing box checksum...
==> default: Successfully added box 'centos/7' (v2004.01) for 'virtualbox'!
==> default: Importing base box 'centos/7'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: CentOS7_default_1639126277923_15247
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it`'s present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.

PS D:\Vagrant\CentOS7> vagrant status
Current machine states:

default                   running (virtualbox)

PS D:\Vagrant\CentOS7> vagrant ssh
[vagrant@localhost ~]$

PS D:\Vagrant\CentOS7> vagrant destroy -f
==> default: Forcing shutdown of VM...
==> default: Destroying VM and associated drives...

PS D:\Vagrant\CentOS7> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos/7'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: CentOS7_default_1639126897568_19555
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it`'s present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
==> default: Running provisioner: shell...
    default: Running: C:/Users/ahache/AppData/Local/Temp/vagrant-shell20211210-12960-1042pff.sh
    default: Loaded plugins: fastestmirror
    default: Determining fastest mirrors
    default:  * base: ftp.pasteur.fr
    default:  * extras: centos.mirrors.proxad.net
    default:  * updates: centos.mirrors.proxad.net
    default: Resolving Dependencies
    default: --> Running transaction check
    default: ---> Package epel-release.noarch 0:7-11 will be installed
    default: --> Finished Dependency Resolution
    default:
    default: Dependencies Resolved
    [...]
    default: Complete!
    default: Loaded plugins: fastestmirror
    default: Cleaning repos: base epel extras updates
    default: Cleaning up list of fastest mirrors

PS D:\Vagrant\CentOS7> vagrant package --output centos-cesi.box
==> default: Attempting graceful shutdown of VM...
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: D:/Vagrant/CentOS7/centos-cesi.box

PS D:\Vagrant\CentOS7> vagrant box add centos-cesi centos-cesi.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos-cesi' (v0) for provider:
    box: Unpacking necessary files from: file://D:/Vagrant/CentOS7/centos-cesi.box
    box:
==> box: Successfully added box 'centos-cesi' (v0) for 'virtualbox'!

PS D:\Vagrant\CentOS7> vagrant box list
centos-cesi (virtualbox, 0)
centos/7    (virtualbox, 2004.01)

PS D:\Vagrant\CentOS7> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Checking if box 'centos/7' version '2004.01' is up to date...
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
==> default: Rsyncing folder: /cygdrive/d/Vagrant/CentOS7/ => /vagrant
==> default: Machine already provisioned. Run `vagrant provision` or use the `--provision`
==> default: flag to force provisioning. Provisioners marked to run always will still run.

PS D:\Vagrant\CentOS7> vagrant status
Current machine states:

default                   running (virtualbox)

PS D:\Vagrant\CentOS7> vagrant ssh
[vagrant@localhost ~]$ sudo yum update
Loaded plugins: fastestmirror
Determining fastest mirrors
[...]
No packages marked for update

[vagrant@localhost ~]$ sudo yum install vim
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.crazyfrogs.org
 * epel: mirror.netweaver.uk
 * extras: centos.mirrors.proxad.net
 * updates: centos.mirrors.proxad.net
Package 2:vim-enhanced-7.4.629-8.el7_9.x86_64 already installed and latest version
Nothing to do
```
## Repackaging Ansible+Vim
###
### `setup.sh`
```bash
#!/bin/bash

# Ajout des dépôts EPEL
yum install -y epel-release

# Mise à jour du système
yum update -y

# On active la connexion au serveur SSH avec un mot de passe
# Ui c'est nul, mais on en a besoin ensuite
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Clean caches and artifacts
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
```

### Vagrantfile
```
Vagrant.configure("2")do|config|
  # Notre box custom
  config.vm.box="centos-cesi"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "1024"
  end

  config.vm.synced_folder ".", "/vagrant", disabled: true  
  config.vm.provision "shell", path: "setup.sh"
end
```

### Vagrant
```powershell
PS D:\Vagrant\CentOS7> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos-cesi'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: CentOS7_default_1639128632260_63088
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.

PS D:\Vagrant\CentOS7> vagrant ssh
[vagrant@localhost ~]$ sudo yum install vim
Loaded plugins: fastestmirror
[...]
Package 2:vim-enhanced-7.4.629-8.el7_9.x86_64 already installed and latest version
Nothing to do

[vagrant@localhost ~]$ sudo yum install ansible
Loaded plugins: fastestmirror
[...]
Installed:
  ansible.noarch 0:2.9.25-1.el7

Dependency Installed:
  PyYAML.x86_64 0:3.10-11.el7           libyaml.x86_64 0:0.1.4-11.el7_0                            python-babel.noarch 0:0.9.6-8.el7
  python-backports.x86_64 0:1.0-8.el7   python-backports-ssl_match_hostname.noarch 0:3.5.0.1-1.el7 python-cffi.x86_64 0:1.6.0-5.el7
  python-enum34.noarch 0:1.0.4-1.el7    python-idna.noarch 0:2.4-1.el7                             python-ipaddress.noarch 0:1.0.16-2.el7
  python-jinja2.noarch 0:2.7.2-4.el7    python-markupsafe.x86_64 0:0.11-10.el7                     python-paramiko.noarch 0:2.1.1-9.el7
  python-ply.noarch 0:3.4-11.el7        python-pycparser.noarch 0:2.14-1.el7                       python-setuptools.noarch 0:0.9.8-7.el7
  python-six.noarch 0:1.9.0-2.el7       python2-cryptography.x86_64 0:1.7.2-2.el7                  python2-httplib2.noarch 0:0.18.1-3.el7
  python2-jmespath.noarch 0:0.9.4-2.el7 python2-pyasn1.noarch 0:0.1.9-7.el7                        sshpass.x86_64 0:1.06-2.el7

Complete!

[vagrant@localhost ~]$ logout
Connection to 127.0.0.1 closed.

PS D:\Vagrant\CentOS7> vagrant package --output centos-cesi.box
==> default: Clearing any previously set forwarded ports...
==> default: Exporting VM...
==> default: Compressing package to: D:/Vagrant/CentOS7/centos-cesi.box

PS D:\Vagrant\CentOS7> vagrant box add --force centos-cesi centos-cesi.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'centos-cesi' (v0) for provider:
    box: Unpacking necessary files from: file://D:/Vagrant/CentOS7/centos-cesi.box
    box:
==> box: Successfully added box 'centos-cesi' (v0) for 'virtualbox'!

PS D:\Vagrant\CentOS7> vagrant up
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'centos-cesi'...
==> default: Matching MAC address for NAT networking...
==> default: Setting the name of the VM: CentOS7_default_1639129454240_64043
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default:
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.
==> default: Running provisioner: shell...
    default: Running: C:/Users/ahache/AppData/Local/Temp/vagrant-shell20211210-8244-131pwqb.sh
    default: Loaded plugins: fastestmirror
    default: Loading mirror speeds from cached hostfile
    default:  * base: ftp.pasteur.fr
    default:  * epel: linuxsoft.cern.ch
    default:  * extras: centos.mirror.fr.planethoster.net
    default:  * updates: ftp.pasteur.fr
    default: Package epel-release-7-14.noarch already installed and latest version
    default: Nothing to do
    default: Loaded plugins: fastestmirror
    default: Loading mirror speeds from cached hostfile
    default:  * base: ftp.pasteur.fr
    default:  * epel: linuxsoft.cern.ch
    default:  * extras: centos.mirror.fr.planethoster.net
    default:  * updates: ftp.pasteur.fr
    default: No packages marked for update
    default: Loaded plugins: fastestmirror
    default: Cleaning repos: base epel extras updates
    default: Cleaning up list of fastest mirrors

PS D:\Vagrant\CentOS7> vagrant ssh
[vagrant@localhost ~]$ sudo yum install vim ansible
Loaded plugins: fastestmirror
Determining fastest mirrors
[...]
Package 2:vim-enhanced-7.4.629-8.el7_9.x86_64 already installed and latest version
Package ansible-2.9.25-1.el7.noarch already installed and latest version
Nothing to do
```

## Trois nodes
###
### Vagrantfile
```
Vagrant.configure("2")do|config|
  # Notre box custom
  config.vm.box="centos-cesi"

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "1024"
  end

  config.vm.synced_folder ".", "/vagrant", disabled: true  
  config.vm.provision "shell", path: "setup.sh"

  config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "10.3.1.1"
  end

  config.vm.define "node2" do |node2|
    node2.vm.network "private_network", ip: "10.3.1.2"
  end

  config.vm.define "node3" do |node3|
    node3.vm.network "private_network", ip: "10.3.1.3"
  end

end
```

### `setup.sh`
```bash
#!/bin/bash

# Ajout des dépôts EPEL
yum install -y epel-release

# Mise à jour du système
yum update -y

# On active la connexion au serveur SSH avec un mot de passe
# Ui c'est nul, mais on en a besoin ensuite
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Clean caches and artifacts
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
```

### Vagrant
```powershell
PS D:\Vagrant\CentOS7> vagrant up
Bringing machine 'node1' up with 'virtualbox' provider...
Bringing machine 'node2' up with 'virtualbox' provider...
Bringing machine 'node3' up with 'virtualbox' provider...
==> node1: You assigned a static IP ending in ".1" to this machine.
    [...]
    node1: Cleaning up list of fastest mirrors
==> node2: Importing base box 'centos-cesi'...
    [...]
    node2: Cleaning up list of fastest mirrors
==> node3: Importing base box 'centos-cesi'...
    [...]
    node3: Cleaning up list of fastest mirrors

PS D:\Vagrant\CentOS7> vagrant status
Current machine states:

node1                     running (virtualbox)
node2                     running (virtualbox)
node3                     running (virtualbox)

PS D:\Vagrant\CentOS7> vagrant ssh node1
[vagrant@localhost ~]$ ping 10.3.1.2
PING 10.3.1.2 (10.3.1.2) 56(84) bytes of data.
64 bytes from 10.3.1.2: icmp_seq=1 ttl=64 time=0.794 ms
64 bytes from 10.3.1.2: icmp_seq=2 ttl=64 time=0.581 ms
^C
--- 10.3.1.2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1000ms
rtt min/avg/max/mdev = 0.581/0.687/0.794/0.109 ms
[vagrant@localhost ~]$ ping 10.3.1.3
PING 10.3.1.3 (10.3.1.3) 56(84) bytes of data.
64 bytes from 10.3.1.3: icmp_seq=1 ttl=64 time=1.29 ms
64 bytes from 10.3.1.3: icmp_seq=2 ttl=64 time=0.582 ms
^C
--- 10.3.1.3 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.582/0.938/1.295/0.357 ms

[vagrant@localhost ~]$ ssh vagrant@10.3.1.2
The authenticity of host '10.3.1.2 (10.3.1.2)' can't be established.
ECDSA key fingerprint is SHA256:LHRRSVFsJC7eGpOtOzPbSfBtPLpilN4uoj2v91Zg6aM.
ECDSA key fingerprint is MD5:65:03:e7:d7:88:cc:bd:ef:86:60:0d:0d:99:59:df:83.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.3.1.2' (ECDSA) to the list of known hosts.
vagrant@10.3.1.2's password:
Last login: Fri Dec 10 09:31:51 2021 from 10.0.2.2
[vagrant@localhost ~]$
```

## Ansible
###
### Vagrantfile
```
servers=[
  {
    :hostname => "admin.tp4.cesi",
    :ip => "10.3.1.10",
    :box => "centos-cesi",
    :ram => 1024,
    :cpu => 1
  },
  {
    :hostname => "node1.tp4.cesi",
    :ip => "10.3.1.1",
    :box => "centos/7",
    :ram => 1024,
    :cpu => 1
  },
  {
    :hostname => "node2.tp4.cesi",
    :ip => "10.3.1.2",
    :box => "centos/7",
    :ram => 1024,
    :cpu => 1
  }
]

Vagrant.configure(2) do |config|
  servers.each do |machine|
    config.vm.define machine[:hostname] do |node|
      config.vm.synced_folder ".", "/vagrant", disabled: true
      node.vm.box = machine[:box]
      node.vm.hostname = machine[:hostname]
      node.vm.network "private_network", ip: machine[:ip]
      config.vm.provision "shell", path: "setup.sh"
      node.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--memory", machine[:ram]]
        vb.gui = false
      end
    end
  end
end

```

### `setup.sh`
```bash
#!/bin/bash

# Ajout des dépôts EPEL
yum install -y epel-release

# Mise à jour du système
yum update -y

# On active la connexion au serveur SSH avec un mot de passe
# Ui c'est nul, mais on en a besoin ensuite
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config

# Clean caches and artifacts
rm -f /etc/udev/rules.d/70-persistent-net.rules # Normalement useless, mais au cas où
yum clean all
rm -rf /tmp/*
rm -f /var/log/wtmp /var/log/btmp
history -c
```

### Vagrant 
```bash
PS D:\\Vagrant\\CentOS7> vagrant ssh admin.tp4.cesi
Last login: Fri Dec 10 09:31:51 2021 from 10.0.2.2
[vagrant@admin ~]$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/vagrant/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/vagrant/.ssh/id_rsa.
Your public key has been saved in /home/vagrant/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:bXxNNrfId89Ob0puOy8xwUagSoHJmiUEfe+CHOB+JyM vagrant@admin.tp4.cesi
The key\'s randomart image is:
+---[RSA 2048]----+
|  .+.. o.   ..   |
|  . o *  . .  .  |
| . . * .. .  o+ .|
|  . +  ..+  .+=o.|
| . . o .S + .+.+.|
|  E * o .. .  +.o|
|   o + .      .o+|
|             o+oo|
|             .+B+|
+----[SHA256]-----+

[vagrant@admin ~]$ logout

PS D:\Vagrant\CentOS7> vagrant ssh node1.tp4.cesi
[vagrant@node1 ~]$ scp vagrant@10.3.1.10:/home/vagrant/.ssh/id_rsa.pub /home/vagrant
The authenticity of host '10.3.1.10 (10.3.1.10)' can\'t be established.
ECDSA key fingerprint is SHA256:LHRRSVFsJC7eGpOtOzPbSfBtPLpilN4uoj2v91Zg6aM.
ECDSA key fingerprint is MD5:65:03:e7:d7:88:cc:bd:ef:86:60:0d:0d:99:59:df:83.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.3.1.10' (ECDSA) to the list of known hosts.
vagrant@10.3.1.10\'s password:
id_rsa.pub                                                                                              100%  404   183.5KB/s   00:00
[vagrant@node1 ~]$ ls .ssh
authorized_keys  known_hosts
[vagrant@node1 ~]$ cat id_rsa.pub >> ./.ssh/authorized_keys
[vagrant@node1 ~]$ chmod 700 .ssh/
[vagrant@node1 ~]$ chmod 600 .ssh/authorized_keys
[vagrant@node1 ~]$ logout

PS D:\Vagrant\CentOS7> vagrant ssh node2.tp4.cesi
[vagrant@node2 ~]$ scp vagrant@10.3.1.10:/home/vagrant/.ssh/id_rsa.pub /home/vagrant
The authenticity of host '10.3.1.10 (10.3.1.10)' can\'t be established.
ECDSA key fingerprint is SHA256:LHRRSVFsJC7eGpOtOzPbSfBtPLpilN4uoj2v91Zg6aM.
ECDSA key fingerprint is MD5:65:03:e7:d7:88:cc:bd:ef:86:60:0d:0d:99:59:df:83.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.3.1.10' (ECDSA) to the list of known hosts.
vagrant@10.3.1.10\'s password:
id_rsa.pub                                                                                              100%  404   161.4KB/s   00:00
[vagrant@node2 ~]$ cat id_rsa.pub >> ./.ssh/authorized_keys
[vagrant@node2 ~]$ chmod 700 .ssh/
[vagrant@node2 ~]$ chmod 600 .ssh/authorized_keys
[vagrant@node2 ~]$ logout

PS D:\Vagrant\CentOS7> vagrant ssh admin.tp4.cesi
Last login: Fri Dec 10 10:54:22 2021 from 10.0.2.2
[vagrant@admin ~]$ mkdir ansible
[vagrant@admin ~]$ cd ansible

[vagrant@admin ansible]$ sudo vim nginx.yml
[vagrant@admin ansible]$ sudo vim hosts.ini
[vagrant@admin ansible]$ sudo vim index.html.j2

[vagrant@admin ansible]$ ansible-playbook -i hosts.ini nginx.yml

PLAY [Install nginx]
[...]
PLAY RECAP *******************************************************************************************************************************
10.3.1.1                   : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
10.3.1.2                   : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


```