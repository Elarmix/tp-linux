# TP2: Un service entouré d'un écosystème riche

## Serveur `proxy`
###
### Sécurisation de SSH
```bash
[ahache@proxy ~]$ sudo vim /etc/ssh/sshd_config
[ahache@proxy ~]$ sudo cat /etc/ssh/sshd_config | grep "PermitRootLogin"
PermitRootLogin no
[ahache@proxy ~]$ sudo cat /etc/ssh/sshd_config | grep "PasswordAuthentication"
PasswordAuthentication no
[ahache@proxy ~]$ sudo cat /etc/ssh/sshd_config
[...]
# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256
```

### Installation et configuration de Fail2Ban
```bash
[ahache@proxy ~]$ sudo dnf install epel-release -y
Rocky Linux 8 - AppStream          2.7 MB/s | 8.3 MB     00:03
Rocky Linux 8 - BaseOS             2.4 MB/s | 3.5 MB     00:01
Rocky Linux 8 - Extras              27 kB/s |  10 kB     00:00
Dependencies resolved.
[...]
Installed:
  epel-release-8-13.el8.noarch

Complete!

[ahache@proxy ~]$ sudo systemctl start firewalld
[ahache@proxy ~]$ sudo systemctl enable firewalld
[ahache@proxy ~]$ sudo systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-12-08 10:04:11 CET; 19min ago
     Docs: man:firewalld(1)
 Main PID: 812 (firewalld)
    Tasks: 2 (limit: 4936)
   Memory: 30.5M
   CGroup: /system.slice/firewalld.service
           └─812 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid

Dec 08 10:04:10 proxy.tp2.cesi systemd[1]: Starting firewalld - dynamic firewall daemon...
Dec 08 10:04:11 proxy.tp2.cesi systemd[1]: Started firewalld - dynamic firewall daemon.
Dec 08 10:04:11 proxy.tp2.cesi firewalld[812]: WARNING: AllowZoneDrifting is enabled. This is considered an insecure configuration option>

[ahache@proxy ~]$ sudo dnf install fail2ban fail2ban-firewalld -y
Extra Packages for Enterprise Linux 8 - x86_64                                                            2.8 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64                                                    958 kB/s | 980 kB     00:01
Last metadata expiration check: 0:00:01 ago on Wed 08 Dec 2021 10:24:54 AM CET.
Dependencies resolved.
[...]
Installed:
  esmtp-1.2-15.el8.x86_64                 fail2ban-0.11.2-1.el8.noarch              fail2ban-firewalld-0.11.2-1.el8.noarch
  fail2ban-sendmail-0.11.2-1.el8.noarch   fail2ban-server-0.11.2-1.el8.noarch       libesmtp-1.0.6-18.el8.x86_64
  liblockfile-1.14-1.el8.x86_64           python3-pip-9.0.3-20.el8.rocky.0.noarch   python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64

Complete!

[ahache@proxy ~]$ sudo systemctl start fail2ban
[ahache@proxy ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[ahache@proxy ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 10:25:52 CET; 6s ago
     Docs: man:fail2ban(1)
 Main PID: 30245 (fail2ban-server)
    Tasks: 3 (limit: 4936)
   Memory: 13.0M
   CGroup: /system.slice/fail2ban.service
           └─30245 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf start

Dec 08 10:25:51 proxy.tp2.cesi systemd[1]: Starting Fail2Ban Service...
Dec 08 10:25:52 proxy.tp2.cesi systemd[1]: Started Fail2Ban Service.
Dec 08 10:25:52 proxy.tp2.cesi fail2ban-server[30245]: Server ready

[ahache@proxy ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

[ahache@proxy ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local

[ahache@proxy ~]$ sudo vim /etc/fail2ban/jail.d/sshd.local
[ahache@proxy ~]$ sudo cat /etc/fail2ban/jail.d/sshd.local
[sshd]
enabled = true
bantime = 1d
maxretry = 3

[ahache@proxy ~]$ sudo systemctl restart fail2ban
[ahache@proxy ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 10:34:15 CET; 7s ago
     Docs: man:fail2ban(1)
  Process: 30364 ExecStop=/usr/bin/fail2ban-client stop (code=exited, status=0/SUCCESS)
  Process: 30365 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 30367 (fail2ban-server)
    Tasks: 3 (limit: 4936)
   Memory: 10.6M
   CGroup: /system.slice/fail2ban.service
           └─30367 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf start

Dec 08 10:34:15 proxy.tp2.cesi systemd[1]: fail2ban.service: Succeeded.
Dec 08 10:34:15 proxy.tp2.cesi systemd[1]: Stopped Fail2Ban Service.
Dec 08 10:34:15 proxy.tp2.cesi systemd[1]: Starting Fail2Ban Service...
Dec 08 10:34:15 proxy.tp2.cesi systemd[1]: Started Fail2Ban Service.
Dec 08 10:34:15 proxy.tp2.cesi fail2ban-server[30367]: Server ready
```

## Test de Fail2Ban

Ordinateur test :
###
```powershell
PS C:\Users\ahache\> ssh root@10.2.1.20
root@10.2.1.20`'s password:
Permission denied, please try again.
root@10.2.1.20`'s password:
Permission denied, please try again.
root@10.2.1.20`'s password:
root@10.2.1.20: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
PS C:\Users\ahache\> ssh root@10.2.1.20
ssh: connect to host 10.2.1.20 port 22: Connection timed out
```

Serveur :
###
```bash
[ahache@proxy ~]$ sudo fail2ban-client banned
[{'sshd': ['10.2.1.100']}]`
```

## Installation et configuration de NGINX
```bash
[ahache@proxy ~]$ sudo dnf install nginx
Last metadata expiration check: 0:34:12 ago on Wed 08 Dec 2021 10:24:54 AM CET.
Dependencies resolved.
[...]
Installed:
  gd-2.2.5-7.el8.x86_64
  jbigkit-libs-2.1-14.el8.x86_64
  [...]
   perl-threads-shared-1.58-2.el8.x86_64

Complete!

[ahache@proxy ~]$ sudo cat /etc/nginx/conf.d/reverse_nextcloud.conf
server {
        listen 80;
        server_name web.tp2.cesi;

        location / {
                proxy_pass http://web.tp2.cesi;
                proxy_http_version  1.1;
                proxy_cache_bypass  $http_upgrade;

                proxy_set_header Upgrade           $http_upgrade;
                proxy_set_header Connection        "upgrade";
                proxy_set_header Host              $host;
                proxy_set_header X-Real-IP         $remote_addr;
                proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header X-Forwarded-Host  $host;
                proxy_set_header X-Forwarded-Port  $server_port;

        }
}

[ahache@proxy ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.2.1.11 web.tp2.cesi

[ahache@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[ahache@proxy ~]$ sudo firewall-cmd --reload
success

[ahache@proxy ~]$ sudo systemctl enable nginx
[ahache@proxy ~]$ sudo systemctl restart nginx
[ahache@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 13:55:30 CET; 2min 38s ago
 Main PID: 1990 (nginx)
    Tasks: 2 (limit: 4936)
   Memory: 3.7M
   CGroup: /system.slice/nginx.service
           ├─1990 nginx: master process /usr/sbin/nginx
           └─1991 nginx: worker process

Dec 08 13:55:30 proxy.tp2.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 08 13:55:30 proxy.tp2.cesi nginx[1986]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 08 13:55:30 proxy.tp2.cesi nginx[1986]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 08 13:55:30 proxy.tp2.cesi systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 08 13:55:30 proxy.tp2.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.
```

## Configuration NGINX en HTTPS
###
```bash
[ahache@proxy ~]$ openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt
Generating a RSA private key
............................................................................................++++
....................................................................................................................................................................................................................................................................................................................................................................................................................++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Gironde
Locality Name (eg, city) [Default City]:Bordeaux
Organization Name (eg, company) [Default Company Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (eg, your name or your server\'s hostname) []:web.tp2.cesi
Email Address []:

[ahache@proxy ~]$ ls
server.crt  server.key

[ahache@proxy ~]$ mv server.crt /etc/pki/tls/certs/web.tp2.cesi.crt
[ahache@proxy ~]$ mv server.key /etc/pki/tls/private/web.tp2.cesi.key

[ahache@proxy ~]$ sudo cat /etc/nginx/conf.d/reverse_nextcloud.conf
server {
        listen 443 ssl http2;
        server_name web.tp2.cesi;

        ssl_certificate /etc/pki/tls/certs/web.tp2.cesi.crt;
        ssl_certificate_key /etc/pki/tls/private/web.tp2.cesi.key;
        [...]
}

[ahache@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[ahache@proxy ~]$ sudo firewall-cmd --reload
success

[ahache@proxy ~]$ sudo systemctl restart nginx
[ahache@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-12-08 14:15:44 CET; 7s ago
  Process: 2229 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 2228 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 2226 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 2231 (nginx)
    Tasks: 2 (limit: 4936)
   Memory: 3.8M
   CGroup: /system.slice/nginx.service
           ├─2231 nginx: master process /usr/sbin/nginx
           └─2232 nginx: worker process

Dec 08 14:15:44 proxy.tp2.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 08 14:15:44 proxy.tp2.cesi nginx[2228]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 08 14:15:44 proxy.tp2.cesi nginx[2228]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 08 14:15:44 proxy.tp2.cesi systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 08 14:15:44 proxy.tp2.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.

[ahache@proxy ~]$ curl -o a -k web.tp2.cesi
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   156  100   156    0     0  22285      0 --:--:-- --:--:-- --:--:-- 22285
```