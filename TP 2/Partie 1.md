# TP2: Un service entouré d'un écosystème riche

## Serveur `db`
###
### Installation de MariaDB
```bash
[ahache@web ~]$ sudo dnf install mariadb-server -y
Last metadata expiration check: 2:54:51 ago on Tue 07 Dec 2021 10:50:44 AM CET.
Package mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!

[ahache@db ~]$ sudo systemctl start mariadb
[ahache@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:03:15 CET; 21min ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
  Process: 1470 ExecStartPost=/usr/libexec/mysql-check-upgrade (code=exited, status=0/SUCCESS)
  Process: 851 ExecStartPre=/usr/libexec/mysql-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
  Process: 813 ExecStartPre=/usr/libexec/mysql-check-socket (code=exited, status=0/SUCCESS)
 Main PID: 893 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 31 (limit: 4936)
   Memory: 107.6M
   CGroup: /system.slice/mariadb.service
           └─893 /usr/libexec/mysqld --basedir=/usr

Dec 07 14:03:10 db.tp2.cesi systemd[1]: Starting MariaDB 10.3 database server...
Dec 07 14:03:11 db.tp2.cesi mysql-prepare-db-dir[851]: Database MariaDB is probably initialized in /var/lib/mysql alrea>
Dec 07 14:03:11 db.tp2.cesi mysql-prepare-db-dir[851]: If this is not the case, make sure the /var/lib/mysql is empty b>
Dec 07 14:03:11 db.tp2.cesi mysqld[893]: 2021-12-07 14:03:11 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-MariaDB) star>
Dec 07 14:03:15 db.tp2.cesi systemd[1]: Started MariaDB 10.3 database server.

[ahache@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

### Port d'écoute de MySQL
```bash
[ahache@db ~]$ sudo ss -ltpn | grep "mysql"
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=893,fd=22))
```

### Utilisateur qui exécute MySQL
```bash
[ahache@db ~]$ sudo ps -eo ruser,comm | grep mysql
mysql    mysqld
```

### Règle firewall pour MySQL
```bash
[ahache@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[ahache@db ~]$ sudo firewall-cmd --reload
success
```

### Configuration de MySQL
```bash
[ahache@db ~]$ mysql_secure_installation
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we\'ll need the current
password for the root user.  If you\'ve just installed MariaDB, and
you haven\'t set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you\'ve completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

### Création du user et de la base NextCloud
```bash
[ahache@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 rows affected (0.001 sec)
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';
Query OK, 0 rows affected (0.001 sec)
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

## Serveur `web`
###

### Installation de mysql
```bash
[ahache@web ~]$ sudo dnf provides mysql
Last metadata expiration check: 0:02:43 ago on Tue 07 Dec 2021 01:57:20 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

[ahache@web ~]$ sudo dnf install mysql -y
Last metadata expiration check: 0:03:03 ago on Tue 07 Dec 2021 01:57:20 PM CET.
Dependencies resolved.
[...]
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch               mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!

[ahache@web ~]$ mysql --host=10.2.1.12 --user='nextcloud' --password nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 13
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW TABLES;
Empty set (0.00 sec)

mysql> exit
Bye
```

## Installation de Apache
###

```bash
[ahache@web ~]$ sudo dnf install httpd
Last metadata expiration check: 0:25:16 ago on Tue 07 Dec 2021 01:57:20 PM CET.
Dependencies resolved.
[...]
Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!

[ahache@web ~]$ sudo systemctl start httpd

[ahache@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-12-07 14:27:49 CET; 7s ago
     Docs: man:httpd.service(8)
 Main PID: 3499 (httpd)
   Status: "Started, listening on: port 80"
    Tasks: 213 (limit: 4936)
   Memory: 25.0M
   CGroup: /system.slice/httpd.service
           ├─3499 /usr/sbin/httpd -DFOREGROUND
           ├─3500 /usr/sbin/httpd -DFOREGROUND
           ├─3501 /usr/sbin/httpd -DFOREGROUND
           ├─3502 /usr/sbin/httpd -DFOREGROUND
           └─3503 /usr/sbin/httpd -DFOREGROUND

Dec 07 14:27:49 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 07 14:27:49 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.
Dec 07 14:27:49 web.tp2.cesi httpd[3499]: Server configured, listening on: port 80

[ahache@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

[ahache@web ~]$ sudo ss -ltpn | grep "httpd"
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=3503,fd=4),("httpd",pid=3502,fd=4),("http",pid=3501,fd=4),("httpd",pid=3499,fd=4))
[ahache@web ~]$ sudo ps -eo ruser,comm | grep "httpd"
root     httpd
apache   httpd
apache   httpd
apache   httpd
apache   httpd

[ahache@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
Warning: ALREADY_ENABLED: 80:tcp
success
[ahache@web ~]$ sudo firewall-cmd --reload
success

[ahache@web ~]$ curl -o ~/apache 10.2.1.11
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7621  100  7621    0     0  7442k      0 --:--:-- --:--:-- --:--:-- 7442k
```

## Installation de PHP
###
```bash
[ahache@web ~]$ sudo dnf install epel-release
Last metadata expiration check: 0:47:31 ago on Tue 07 Dec 2021 01:57:20 PM CET.
Dependencies resolved.
[...]
Installed:
  epel-release-8-13.el8.noarch

Complete!

[ahache@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                          2.8 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64                                  808 kB/s | 980 kB     00:01
Last metadata expiration check: 0:00:01 ago on Tue 07 Dec 2021 02:46:01 PM CET.
Dependencies resolved.
[...]
Transaction Summary
========================================================================================================================
Install   3 Packages
Upgrade  33 Packages

Total download size: 116 M
[...]
Installed:
  kernel-4.18.0-348.2.1.el8_5.x86_64 kernel-core-4.18.0-348.2.1.el8_5.x86_64 kernel-modules-4.18.0-348.2.1.el8_5.x86_64

Complete!

[ahache@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:03:19 ago on Tue 07 Dec 2021 02:46:01 PM CET.
remi-release-8.rpm                                                                      179 kB/s |  26 kB     00:00
Dependencies resolved.
[...]
Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!

[ahache@web ~]$ dnf module enable php:remi-7.4
Error: This command has to be run with superuser privileges (under the root user on most systems).
[ahache@web ~]$ sudo dnf module enable php:remi-7.4
Remi\'s Modular repository for Enterprise Linux 8 - x86_64                               1.9 kB/s | 858  B     00:00
Remi\'s Modular repository for Enterprise Linux 8 - x86_64                               3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi\'s Modular repository for Enterprise Linux 8 - x86_64                               1.8 MB/s | 946 kB     00:00
Safe Remi\'s RPM repository for Enterprise Linux 8 - x86_64                              3.7 kB/s | 858  B     00:00
Safe Remi\'s RPM repository for Enterprise Linux 8 - x86_64                              3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi\'s RPM repository for Enterprise Linux 8 - x86_64                              2.6 MB/s | 2.0 MB     00:00
Last metadata expiration check: 0:00:01 ago on Tue 07 Dec 2021 02:50:22 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                     Architecture               Version                       Repository                   Size
========================================================================================================================
Enabling module streams:
 php                                                    remi-7.4

Transaction Summary
========================================================================================================================

Is this ok [y/N]: y
Complete!

[ahache@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:00:33 ago on Tue 07 Dec 2021 02:50:22 PM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
[...]
Installed:
  environment-modules-4.5.2-1.el8.x86_64                      gd-2.2.5-7.el8.x86_64
  jbigkit-libs-2.1-14.el8.x86_64                              libXpm-3.5.12-8.el8.x86_64
  libicu69-69.1-1.el8.remi.x86_64                             libjpeg-turbo-1.5.3-12.el8.x86_64
  libsodium-1.0.18-2.el8.x86_64                               libtiff-4.0.9-20.el8.x86_64
  libwebp-1.0.0-5.el8.x86_64                                  oniguruma5php-6.9.7.1-1.el8.remi.x86_64
  php74-libzip-1.8.0-1.el8.remi.x86_64                        php74-php-7.4.26-1.el8.remi.x86_64
  php74-php-bcmath-7.4.26-1.el8.remi.x86_64                   php74-php-cli-7.4.26-1.el8.remi.x86_64
  php74-php-common-7.4.26-1.el8.remi.x86_64                   php74-php-fpm-7.4.26-1.el8.remi.x86_64
  php74-php-gd-7.4.26-1.el8.remi.x86_64                       php74-php-gmp-7.4.26-1.el8.remi.x86_64
  php74-php-intl-7.4.26-1.el8.remi.x86_64                     php74-php-json-7.4.26-1.el8.remi.x86_64
  php74-php-mbstring-7.4.26-1.el8.remi.x86_64                 php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64
  php74-php-opcache-7.4.26-1.el8.remi.x86_64                  php74-php-pdo-7.4.26-1.el8.remi.x86_64
  php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64                 php74-php-process-7.4.26-1.el8.remi.x86_64
  php74-php-sodium-7.4.26-1.el8.remi.x86_64                   php74-php-xml-7.4.26-1.el8.remi.x86_64
  php74-runtime-1.0-3.el8.remi.x86_64                         scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```

## Configuration de Apache/PHP
###

### Dossier de drop-in

`[ahache@web ~]$ cat /etc/httpd/conf/httpd.conf` :
```bash
[...]
# Supplemental configuration
#
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

### Création d'un fichier drop-in

```bash
[ahache@web ~]$ sudo nano /etc/httpd/conf.d/nextcloud.conf
[ahache@web ~]$ cat /etc/httpd/conf.d/nextcloud.confextcloud.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>

[ahache@web ~]$
```

### Nextcloud

```bash
[ahache@web ~]$ sudo mkdir /var/www/nextcloud
[ahache@web ~]$ sudo mkdir /var/www/nextcloud/html

[ahache@web ~]$ sudo chown -R apache /var/www/nextcloud

[ahache@web ~]$ timedatectl
               Local time: Tue 2021-12-07 15:04:12 CET
           Universal time: Tue 2021-12-07 14:04:12 UTC
                 RTC time: Tue 2021-12-07 14:04:12
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no

[ahache@web ~]$ sudo cat /etc/opt/remi/php74/php.ini | grep "date.timezone"
; http://php.net/date.timezone
date.timezone = "Europe/Paris"

[ahache@web ~]$ cd
[ahache@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  1109k      0  0:02:16  0:02:16 --:--:-- 1102k

[ahache@web ~]$ sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud
[...]
[ahache@web ~]$ sudo mv /var/www/nextcloud/nextcloud/* /var/www/nextcloud/html/
[ahache@web ~]$ sudo rm -rf /var/www/nextcloud/nextcloud
[ahache@web ~]$ sudo chown -R apache /var/www/nextcloud
[ahache@web ~]$ rm nextcloud*.zip
```