# TP2: Un service entouré d'un écosystème riche

## Serveur `proxy`
###
### Installation de NetData
```bash
[ahache@proxy ~]$ bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 --- Downloading static netdata binary: https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run ---
[/tmp/netdata-kickstart-eygs2FCB9Q]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-eygs2FCB9Q/sha256sum.txt https://storage.googleapis.com/netdata-nightlies/sha256sums.txt
 OK

[/tmp/netdata-kickstart-eygs2FCB9Q]$ curl -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-eygs2FCB9Q/netdata-latest.gz.run https://storage.googleapis.com/netdata-nightlies/netdata-latest.gz.run
 OK

 --- Installing netdata ---
[...]

[ahache@proxy ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[ahache@proxy ~]$ sudo firewall-cmd --reload
success

[ahache@proxy plugins.d]$ cd /opt/netdata/usr/libexec/netdata/plugins.d/
[ahache@proxy plugins.d]$ ./alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-12-08 14:55:27: alarm-notify.sh: INFO: sent discord notification for: proxy.tp2.cesi test.chart.test_alarm is WARNING to 'slt-mon-reuf'
2021-12-08 14:55:32: alarm-notify.sh: INFO: sent email notification for: proxy.tp2.cesi test.chart.test_alarm is WARNING to 'root'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-12-08 14:55:32: alarm-notify.sh: INFO: sent discord notification for: proxy.tp2.cesi test.chart.test_alarm is CRITICAL to 'slt-mon-reuf'
2021-12-08 14:55:37: alarm-notify.sh: INFO: sent email notification for: proxy.tp2.cesi test.chart.test_alarm is CRITICAL to 'root'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-12-08 14:55:38: alarm-notify.sh: INFO: sent discord notification for: proxy.tp2.cesi test.chart.test_alarm is CLEAR to 'slt-mon-reuf'
2021-12-08 14:55:43: alarm-notify.sh: INFO: sent email notification for: proxy.tp2.cesi test.chart.test_alarm is CLEAR to 'root'
# OK
```