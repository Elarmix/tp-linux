# TP3: Docker

## Serveur `web`
###
### Docker
```bash
[ahache@web ~]$ sudo yum install -y yum-utils
[...]
Installed:
  yum-utils-4.0.21-3.el8.noarch

Complete!

[ahache@web ~]$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo

[ahache@web ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
Last metadata expiration check: 0:06:43 ago on Thu 09 Dec 2021 02:09:40 PM CET.
Package docker-ce-3:20.10.11-3.el8.x86_64 is already installed.
Package docker-ce-cli-1:20.10.11-3.el8.x86_64 is already installed.
Package containerd.io-1.4.12-3.1.el8.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!

[ahache@web ~]$ sudo systemctl start docker
[ahache@web ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[ahache@web ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-12-09 14:17:12 CET; 9s ago
     Docs: https://docs.docker.com
 Main PID: 40722 (dockerd)
    Tasks: 7
   Memory: 31.3M
   CGroup: /system.slice/docker.service
           └─40722 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

[ahache@web ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.6.3-docker)
  scan: Docker Scan (Docker Inc., v0.9.0)
  [...]

[ahache@web ~]$ docker run -p 8080:80 -d -v /srv/nginx_docker/nginx_conf/nginx.conf:/etc/nginx/nginx.conf -v /srv/nginx_docker/html/index.html:/var/www/html/cesi/index.html nginx
2c8ff2b0e3b0693174d1cd31bdfb32733ba1f3506035eb866ace20c8d78fa3c7

[ahache@web ~]$ docker exec -it 4038a58ae436 bash
root@4038a58ae436:/#

[ahache@web ~]$ docker inspect ae7b2297338c | grep IP
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
                    "IPAMConfig": null,
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,

docker run --memory=128M -p 8080:80 -d -v /srv/nginx_docker/nginx_conf/nginx.conf:/etc/nginx/nginx.conf -v /srv/nginx_docker/html/index.html:/var/www/html/cesi/index.html nginx
cc3fa11008014a2061603422a440d196df40e0eefd1710acfa4746d83fb3a0b2

CONTAINER ID   NAME                     CPU %     MEM USAGE / LIMIT   MEM %     NET I/O     BLOCK I/O         PIDS
cc3fa1100801   infallible_ardinghelli   0.00%     28.5MiB / 128MiB    22.26%    976B / 0B   8.19kB / 8.19kB   2
```

### Applications
```bash
[ahache@web ~]$ docker network create wiki
3522ab1feadb6c668130f95bf001249ae8ef7e58a41d204af95adee8fe48f91a

[ahache@web ~]$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
1db399673a9f   bridge    bridge    local
02ec1085a253   host      host      local
830eb39fb9b4   none      null      local
3522ab1feadb   wiki      bridge    local

[ahache@web ~]$ docker run --network wiki --name MySQL -e MYSQL_ROOT_PASSWORD=azerty -e MYSQL_DATABASE=wiki -e MYSQL_USER=mysql -e MYSQL_PASSWORD=meow -d mysql
Unable to find image 'mysql:latest' locally
latest: Pulling from library/mysql
[...]
04e79350df2356122de49c610b1fb896e3b18cd0e61e8d58ed9dd2423241e4f2
[ahache@web ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                                                    NAMES
04e79350df23   mysql     "docker-entrypoint.s…"   36 seconds ago   Up 32 seconds   33060/tcp, 0.0.0.0:33060->3306/tcp, :::33060->3306/tcp   MySQL

[ahache@web ~]$ docker exec -it 04e79350df23 bash
root@04e79350df23:/# mysql -u mysql -p wiki
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 11
Server version: 8.0.27 MySQL Community Server - GPL

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> exit
Bye

[ahache@web ~]$ sudo docker run -d --network wiki -p 9080:3000 --name wikijs  -e "DB_TYPE=mysql" -e "DB_HOST=localhost" -e "DB_PORT=3306" -e "DB_USER=wiki" -e "DB_PASS=meow" -e "DB_NAME=wiki" requarks/wiki:2
Unable to find image 'requarks/wiki:2' locally
2: Pulling from requarks/wiki
[...]
c7b74daf3494cbc8a8d99f62ef6411253cbbfc7d3c4b995ecfeac906085f4f93

[ahache@web ~]$ docker run -d --network wiki -p 9080:3000 --name wikijs  -e "DB_TYPE=mysql" -e "DB_HOST=localhost" -e "DB_PORT=3306" -e "DB_USER=mysql" -e "DB_PASS=meow" -e "DB_NAME=wiki" requarks/wiki:2
6b18de1ff5c70d96d0fb0c3368affbd2bfb4856367c0bc52c6b178aa28edc726
```